FROM openjdk:8-jre-alpine
VOLUME /tmp
ENV SPRING_PROFILES_ACTIVE docker

ADD build/libs/discovery-server-0.0.1-SNAPSHOT.jar app.jar

RUN /bin/sh -c 'touch app.jar'

ENV app_name "Discovery server"

EXPOSE 8080

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
